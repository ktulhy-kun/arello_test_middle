#!/usr/bin/env python3.5

from unittest import TestCase, main
import json

_data = {
    1534622: {
        'id': '1534622',
        'book_name': 'Война и мир',
        'book_author': 'Л.Н. Толстой',
        'creationDate': '1994-01-02'
    },
    321415: {
        'id': '321415',
        'book_name': 'Преступление и наказание',
        'book_author': 'Ф.М. Достоевский',
        'creationDate': '2004-07-08'
    }
}


class Server:
    class __Server:

        def _generate_response(self, code: int, data: dict) -> dict:
            return {
                'response': {
                    'code': code,
                    'data': data
                }
            }

        def _server_side_obj(self, query: dict) -> dict:
            _id = query.get('id', None)  # type: str
            try:
                _id = int(_id)
            except TypeError:
                return self._generate_response(400, {})

            _id = int(_id)

            if _id in _data:
                return self._generate_response(200, _data[_id])
            else:
                return self._generate_response(404, {})

        def send_json(self, raw_query: str) -> str:
            query = json.loads(raw_query)
            answer = self._server_side_obj(query)
            return json.dumps(answer)

    instance = None

    def __init__(self):
        if not Server.instance:
            Server.instance = Server.__Server()

    def __getattr__(self, item):
        return getattr(self.instance, item)


class ServerAdapter:
    def __init__(self, server: Server = None):
        self.server = server or Server()

    def send_raw_query(self, raw_query: str) -> dict:
        answer = self.server.send_json(raw_query)
        return json.loads(answer)


class TestAPI(TestCase):

    def _generate_queries(self):
        obj_queries =  [{"id": _id} for _id in list(_data.keys())]
        obj_queries = [{"id": str(_id)} for _id in list(_data.keys())] + obj_queries

        correct_id = str(list(_data.keys())[0])
        obj_queries.append({
            "id": correct_id,
            "baldfafds": "afiuqhg3498h341"
        })
        obj_queries.append({
            "id": "            " + correct_id
        })
        obj_queries.append({
            "id": correct_id + "            "
        })
        obj_queries.append({
            "id": "            " + correct_id + "            "
        })
        obj_queries.append({
            "id": "1534622",
            "இ": "cool unicode symbol"
        })

        obj_queries.append({
            "id": "1534622",
            "Unicode Symbol in data": "私はあなたを破るよ、スカム"
        })
        obj_queries.append({
            "id": "1534622",
            "array": [1, 2, 3, 4]
        })
        obj_queries.append({
            "id": "1534622",
            "dict": {
                "lol": "kek",
                "azaza": "ololo"
            }
        })
        obj_queries.append({
            "id": "1534622",
            "cunning dict": {
                "id": "321415",
                "azaza": "ololo"
            }
        })
        obj_queries.append({
            "id": "1534622",
            "id ": "321415",
            " id": "321416",
            " id ": "321416"
        })

        raw_queries = [
            '{"id": 1534622.0e0}',
            '{"id": 153462200e-002}',
            '{"id": "\\u0031\\u0035\\u0033\\u0034\\u0036\\u0032\\u0032"}',
        ]

        return [json.dumps(query) for query in obj_queries] + raw_queries

    def setUp(self):
        self.server_adapter = ServerAdapter()

    def test_correct(self):
        raw_queries = self._generate_queries()

        for raw_query in raw_queries:
            with self.subTest(raw_query=raw_query):
                book_id = json.loads(raw_query)['id']
                r = answer = self.server_adapter.send_raw_query(raw_query)
                self.assertIsInstance(r, dict)
                self.assertIn('response', r)
                self.assertIsInstance(r['response'], dict)

                rr = r['response']
                self.assertIsInstance(rr, dict)

                self.assertIn('code', rr)
                self.assertIsInstance(rr['code'], int)
                self.assertEqual(rr['code'], 200)

                self.assertIn('data', rr)
                self.assertIsInstance(rr['data'], dict)

                self.assertDictEqual(rr['data'], _data[int(book_id)])


if __name__ == '__main__':
    main()
